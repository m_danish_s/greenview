Portfolios = new Mongo.Collection('Portfolios');

if(Meteor.isServer) {
  Meteor.publish('portfolios', function(){
    return Portfolios.find();
  });
}

Meteor.methods({
  addPortfolio:function(name){
    if(Meteor.userId()){
      
      Portfolios.insert({
        portfolioName : name,
        userid: Meteor.userId(),
        buildings: []
      });
    }
  },
  deletePortfolio: function(portfolioId) {
    Portfolios.remove({_id:portfolioId});
  },

  addBuilding: function(portfolioId, building) {
    Portfolios.update({_id: portfolioId}, {$push: {buildings: {name: building.name, owner: building.owner, address: building.address, units: building.units}}});
  },

  deleteBuilding: function(portfolioId, buildingName) {
    Portfolios.update({"buildings.name": buildingName}, {$pull : {buildings: {"name": {$eq: buildingName}}}});
  }
});
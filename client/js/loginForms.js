Template.signup.events({
	"click .showSignin": function(event){
		event.preventDefault();
		Session.set('isSigningUp', false);
	},
	"submit .signup": function(event){
		event.preventDefault();
		var form = event.target;
		Accounts.createUser({
			username: form.username.value,
			password: form.password.value,
			profile:{
				firstname: form.firstname.value,
				lastname: form.lastname.value,
				phone: form.phone.value
			}
		},function(error){
			//do nothing
		});
	}
});

Template.signin.events({
	"click .showSignup": function(event){
		event.preventDefault();
		Session.set('isSigningUp', true);
	},
	"submit .login":function(event){
		event.preventDefault();
		var form = event.target;
		Meteor.loginWithPassword(form.username.value, form.password.value, function(error){
			//do nothing
		});
	}
});
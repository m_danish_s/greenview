Portfolios = new Mongo.Collection('Portfolios');
Buildings = new Mongo.Collection('Buildings');
Units = new Mongo.Collection('Units');

Meteor.subscribe('portfolios');
Meteor.subscribe('buildings', (Session.get('selectedPortfolio'))? Session.get('selectedPortfolio') :0);
Meteor.subscribe('units', (Session.get('selectedBuilding'))? Session.get('selectedBuilding') :0);

Template.body.helpers({
	'isSigningUp': function(){
		return Session.get('isSigningUp');
	}
});

Template.body.events({
	"click .logoutLink":function(event){
		Meteor.logout();
		Session.set('isSigningUp', false);
		Session.set('selectedBuilding', null);
		Session.set('selectedPortfolio',null);
		Session.set('isEdit',false);
		Session.set('selectedUnit', null);
	}
});
Template.owner.helpers({
	portfolios: function(){
		return Portfolios.find();
	},
	selectedPortfolio: function(){
		var portfolio = Session.get('selectedPortfolio');
		if(portfolio)
			return portfolio.portfolioName;

	},
	buildings: function(){
		var portfolio = Session.get('selectedPortfolio');
		if(portfolio){
			return Buildings.find({portfolioId: portfolio._id});
		}
		return null;
	},
	selectedBuilding: function(){
		var building = Session.get('selectedBuilding');
		if(building)
			return building.name;
	},
	buildingUnits: function(){
		var building = Session.get('selectedBuilding');
		if(building)
			return Units.find({buildingId: building._id});
		return null;
	}
});

Template.owner.events({
	"click .addPortfolio": function (){
		Session.set('isEdit', false);
	},
	"click .deletePortfolio": function (){
		Meteor.call('deletePortfolio',this._id);
	},
	"click .editPortfolio": function (){
		Session.set('isEdit', true);
	},
	"click .portfolioLink" : function(event){
		event.preventDefault();
		Session.set('selectedPortfolio', this);
	},
	"click .addBuilding": function (){
		Session.set('isEdit', false);
	},
	"click .editBuilding": function (){
		Session.set('isEdit', true);
		Session.set('selectedBuilding', this);
	},
	"click .deleteBuilding":function(){
		Session.set('selectedBuilding', this);
		Meteor.call('deleteBuilding', this._id);
	},
	"click .buildingLink": function(){
		Session.set('selectedBuilding', this);
	},
	"click .addUnit": function(){
		Session.set('isEdit', false);
		
	},
	"click .editUnit":function(){
		Session.set('isEdit', true);	
		Session.set('selectedUnit', this);
	},
	"click .deleteUnit":function(){
		Meteor.call('deleteUnit', this._id);
	},
});

Template.addPortfolio.helpers({
	isEdit: function() {
		return Session.get('isEdit');
	},
	selectedPortfolio: function(){
		return (Session.get('selectedPortfolio')) ? Session.get('selectedPortfolio').portfolioName : "";
	}
		
});

Template.addPortfolio.events({
	"submit .frmAddPortfolio": function (event) {
		event.preventDefault();
		if(Session.get('isEdit'))
			Meteor.call('editPortfolio', Session.get('selectedPortfolio')._id, event.target.portfolioName.value);
		else 
			Meteor.call('addPortfolio', event.target.portfolioName.value);
		$('#addPortfolioModal').modal('hide');
		$('#frmAddPortfolio')[0].reset();
		window.location.hash = "";
	}
});

Template.addBuilding.events({
	"submit .frmAddBuilding": function (event) {
		event.preventDefault();
		var form = event.target;

		if(Session.get('isEdit'))
			Meteor.call('editBuilding', Session.get('selectedBuilding')._id, {name: form.buildingName.value, address: form.address.value, owner: form.ownerName.value});
		else
			Meteor.call('addBuilding', Session.get('selectedPortfolio')._id, {name: form.buildingName.value, address: form.address.value, owner: form.ownerName.value});
		$('#addBuildingModal').modal('hide');
		$('#frmAddBuilding')[0].reset();
	}
});

Template.addBuilding.helpers({
	isEdit: function() {
		return Session.get('isEdit');
	},
	name: function(){
		return (Session.get('selectedBuilding')) ? Session.get('selectedBuilding').name: "";
	},
	owner: function(){
		return (Session.get('selectedBuilding')) ? Session.get('selectedBuilding').owner: "";
	},
	address: function(){
		return (Session.get('selectedBuilding')) ? Session.get('selectedBuilding').address: "";
	} 
});

Template.addUnit.events({
	"submit .frmUnit": function(event){
		event.preventDefault();
		var form = event.target;
		Meteor.call('addUnit', 
			Session.get('selectedBuilding')._id, 
			{
				unitName: form.unitName.value, 
				primaryTenant: form.primaryTenant.value, 
				tenantEmail: form.tenantEmail.value,
				tenantPassword: form.tenantPassword.value,
				residents: form.residents.value, 
				area: form.area.value
			}, function(error, result) {
				if(!error){
					$('#frmUnit')[0].reset();
					$('#addUnitModal').modal('hide');
					Session.set('tenantError', null);
				}else{
					Session.set('tenantError', error.reason);
				}
			});
	}
});

Template.addUnit.helpers({
	isEdit: function() {
		return Session.get('isEdit');
	},
	tenantError: function(){
		return Session.get('tenantError');
	},
	selectedUnit: function(){
		return Session.get('selectedUnit');
	}
});

Template.editUnit.helpers({
	tenantError: function(){
		return Session.get('tenantError');
	},
	selectedUnit: function(){
		return Session.get('selectedUnit');
	}
});

Template.editUnit.events({
	"submit .frmEditUnit" : function(event) {

		event.preventDefault();
		var form = event.target;
		Meteor.call('editUnit',Session.get('selectedUnit')._id,{
				unitName: form.unitName.value, 
				primaryTenant: form.primaryTenant.value, 
				residents: form.residents.value, 
				area: form.area.value,
				tenantEmail: form.tenantEmail.value,
				tenantPassword: form.tenantPassword.value
			}, function(error, result) {
				if(!error) {
					document.getElementById('editUnitPassword').value = "";
					$('#editUnitModal').modal('hide');
				}else{
					Session.set('tenantError', error.reason);
				}
			});
	}
});


Template.ownerProfile.events({
	"submit .frmUpdateProfile": function(event) {
		event.preventDefault();
		var form = event.target;
		Meteor.call('updateUserProfile', form.firstname.value, form.lastname.value, form.phone.value);
	}
});

Template.tenant.events({
	"submit .frmTenantProfile": function(event) {
		event.preventDefault();
		var form = event.target;
		Meteor.call('updateUserProfile', form.firstname.value, form.lastname.value, form.phone.value);
	}
});
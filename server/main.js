Portfolios = new Mongo.Collection('Portfolios');
Buildings = new Mongo.Collection('Buildings');
Units = new Mongo.Collection('Units');

if(Meteor.isServer) {
  Meteor.publish('portfolios', function(){
    return Portfolios.find({userid: this.userId});
  });

  Meteor.publish('buildings', function(){
    return Buildings.find();
  });

  Meteor.publish('units', function(){
      return Units.find();
  });
}

Meteor.methods({
  addPortfolio:function(name){
    if(Meteor.userId()){
      
      Portfolios.insert({
        portfolioName : name,
        userid: Meteor.userId(),
      });
    }
  },
  editPortfolio: function(portfolioId, name){
    Portfolios.update({_id:portfolioId}, {$set : {portfolioName: name}});
  },
  deletePortfolio: function(portfolioId) {
    Portfolios.remove({_id:portfolioId});
  },

  addBuilding: function(portfolioId, building) {
    Buildings.insert({
      portfolioId: portfolioId,
      name: building.name,
      owner: building.owner,
      address: building.address,
    });
    
  },
  editBuilding: function(buildingId, building) {
    Buildings.update({_id: buildingId}, {$set: {name: building.name, owner: building.owner, address: building.address}});
  },
  deleteBuilding: function(buildingId) {
    Buildings.remove({_id: buildingId});
  },

  addUnit: function(buildingId, unit, callback){
    unit.buildingId = buildingId;
    var err = null;

    if(Accounts.findUserByEmail(unit.tenantEmail)){ //tenant exists
      delete unit.tenantPassword;
      Units.insert(unit);
    }else{
      var newUserId = Accounts.createUser(
      {
        email: unit.tenantEmail, 
        password: unit.tenantPassword, 
        profile:{
          firstname: unit.primaryTenant, 
          lastname:"", 
          phone:"",
          email:unit.tenantEmail,
          isTenant:true
        }
      });
      if(newUserId) {
        delete unit.tenantPassword;
        Units.insert(unit);
      }
    }
    
      

  },
  editUnit: function(unitId, unit){
    //if the email entered already exists update the email and password
    var updatedTenant = Accounts.findUserByEmail(unit.tenantEmail);
    if(updatedTenant){
      
      if(unit.tenantPassword && unit.tenantPassword  !== ""){
        Accounts.setPassword(updatedTenant._id, unit.tenantPassword);
      }
      
      Units.update({_id: unitId}, 
        {
          $set :{
            unitName:unit.unitName, 
            primaryTenant:unit.primaryTenant, 
            residents: unit.residents, 
            area: unit.area,
            tenantEmail: unit.tenantEmail
          }
        });
    }else{
      //if email doesn't exist already, create a new user first
      var newUserId = Accounts.createUser(
      {
        email: unit.tenantEmail, 
        password: unit.tenantPassword, 
        profile:{
          firstname: unit.primaryTenant, 
          lastname:"", 
          phone:"", 
          isTenant:true
        }
      });
      if(newUserId){
         Units.update({_id: unitId}, 
         {
          $set :{
            unitName:unit.unitName, 
            primaryTenant:unit.primaryTenant, 
            residents: unit.residents, 
            area: unit.area,
            tenantEmail:unit.tenantEmail
          }
        });
      }
    }
  },
  deleteUnit: function(unitId){
    Units.remove({_id:unitId});
  },

  updateUserProfile: function(firstname, lastname, phone){
    Meteor.users.update({_id:Meteor.userId()}, {$set : {"profile.firstname": firstname, "profile.lastname": lastname, "profile.phone":phone}});
  }

});